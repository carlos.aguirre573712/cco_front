import { lazy } from 'react';
import FormPqr from '../pages/Form/PQR';
import PqrTable from '../pages/Consultas/PqrTable';
import Pqr from '../pages/Consultas/Pqr';
import Ayuda from '../pages/Ayuda';
import Soporte from '../pages/Soporte';

const Home = lazy(() => import('../pages/Dashboard/Home'));
const Calendar = lazy(() => import('../pages/Calendar'));
const Chart = lazy(() => import('../pages/Chart'));
const FormElements = lazy(() => import('../pages/Form/FormElements'));
const FormLayout = lazy(() => import('../pages/Form/FormLayout'));
const Profile = lazy(() => import('../pages/Profile'));
const Settings = lazy(() => import('../pages/Settings'));
const Tables = lazy(() => import('../pages/Tables'));
const Alerts = lazy(() => import('../pages/UiElements/Alerts'));
const Buttons = lazy(() => import('../pages/UiElements/Buttons'));

const coreRoutes = [
  {
    path: '/home',
    title: 'Home',
    component: Home,
  },

  {
    path: '/calendar',
    title: 'Calender',
    component: Calendar,
  },
  {
    path: '/profile',
    title: 'Profile',
    component: Profile,
  },
  {
    path: '/forms/form-elements',
    title: 'Forms Elements',
    component: FormElements,
  },
  {
    path: '/forms/form-layout',
    title: 'Form Layouts',
    component: FormLayout,
  },
  {
    path: '/forms/form-pqr',
    title: 'Crear PQR',
    component: FormPqr,
  },

  {
    path: '/pqrs',
    title: 'Mis PQR',
    component: PqrTable,
  },
  {
    path: '/pqr',
    title: 'PQR',
    component: Pqr,
  },
  {
    path: '/settings',
    title: 'Settings',
    component: Settings,
  },
  {
    path: '/chart',
    title: 'Chart',
    component: Chart,
  },
  {
    path: '/ui/alerts',
    title: 'Alerts',
    component: Alerts,
  },
  {
    path: '/ui/buttons',
    title: 'Buttons',
    component: Buttons,
  },
  {
    path: '/ayuda',
    title: 'Ayuda',
    component: Ayuda,
  },
  {
    path: '/support',
    title: 'Buttons',
    component: Soporte,
  },
];

const routes = [...coreRoutes];
export default routes;
