
import { Link } from 'react-router-dom';

const Pqr = () => {
  return (
    <>
      <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
        <div className="flex flex-wrap items-center">
          <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
            <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">

              <div className="mb-1.5 flex flex-1 w-64 gap-1 xl:flex-row text-1xl font-semibold text-black dark:text-white" >

                <div className="w-full xl:w-1/2">
                  <h2 className="mb-2 text-2xl  flex justify-left font-bold text-black dark:text-white sm:text-title-xl2">
                    PQRs
                  </h2>
                </div>
                <div className="w-full xl:w-1/2">
                  <h2 className="mb-9 text-2xl  flex justify-left font-bold text-black dark:text-white sm:text-title-xl2">
                    #001
                  </h2>
                </div>
              </div>

              <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">

                <div className="flex flex-wrap items-center">
                  <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
                    <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">

                      <h3 className="mb-6.5  text-2xl font-semibold text-black dark:text-white">
                        Información
                      </h3>

                      <div className="mb-1.5 flex flex-col gap-2 xl:flex-row text-1xl font-semibold text-black dark:text-white" >

                        <div className="w-full xl:w-1/2">Fecha Creación</div>
                        <div className="w-full xl:w-1/2">Estado:</div>


                      </div>
                      <hr />
                      <div className="mb-1.5 flex flex-col gap-2 xl:flex-row text-1xl font-semibold text-black dark:text-white" >

                        <div className="w-full xl:w-1/2"> Fecha Respuesta</div>
                        <div className="w-full xl:w-1/2">TipoPqr:</div>


                      </div>
                      <hr />
                      <div className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Contenido
                      </div>
                      <hr />
                      <div className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Observaciones
                      </div>
                      <p className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque odit perspiciatis at quae. Fugiat natus id delectus, aperiam possimus neque doloremque quisquam aliquam eveniet, quae, quas in commodi. Ipsam, ratione?
                      </p>

                      <hr />
                    </div>

                  </div>
                </div>
              </div>




            </div>
            <div className="mb-10 relative rounded-xl overflow-auto p-1">
              <div className='flex flex-wrap gap-4 items-center justify-center' >
                <div className="w-2/5 flex-none items-center justify-center last:pr-8 ">
                  <Link to="/auth/signup" className="text-primary">
                    <button
                      type="button"
                      className="p-4 w-full  cursor-pointer rounded-lg border border-primary bg-primary text-white transition hover:bg-opacity-90"
                    >
                      Editar
                    </button>
                  </Link>
                </div>
                <div className="w-2/5 flex-none items-center justify-center last:pr-8">
                  <Link to="/auth/signup" className="text-primary">
                    <button
                      type="button"
                      className="p-4 w-full cursor-pointer rounded-lg border border-primary bg-primary text-white transition hover:bg-opacity-90"
                    >
                      Cancelar
                    </button>
                  </Link>
                </div>
              </div>

            </div>








          </div>
        </div>

      </div >
      {/* <div className="grid grid-cols-1 gap-4 md:grid-cols-2 md:gap-6 xl:grid-cols-4 2xl:gap-7.5">
        <CardOne />
        <CardTwo />
        <CardThree />
        <CardFour />
      </div>

      <div className="mt-4 grid grid-cols-12 gap-4 md:mt-6 md:gap-6 2xl:mt-7.5 2xl:gap-7.5">
        <ChartOne />
        <ChartTwo />
        <ChartThree />
        <MapOne />
        <div className="col-span-12 xl:col-span-8">
          <TableOne />
        </div>
        <ChatCard />
      </div> */}
    </>
  );
};

export default Pqr;
