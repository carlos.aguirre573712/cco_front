
import { Link } from 'react-router-dom';

const Ayuda = () => {
  return (
    <>
      <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
        <div className="flex flex-wrap items-center">
          <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
            <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">

              <div className="mb-1.5 flex flex-1 w-64 gap-1 xl:flex-row text-1xl font-semibold text-black dark:text-white" >

                <div className="w-full xl:w-1/2">
                  <h2 className="mb-22 text-2xl  flex justify-left font-bold text-black dark:text-white sm:text-title-xl2">
                    Ayuda
                  </h2>
                </div>

              </div>

              <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">

                <div className="flex flex-wrap items-center">
                  <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
                    <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">


                      <div className="mb-12.5 text-1xl font-semibold text-black dark:text-white">
                        Preguntas frecuentes
                      </div>

                      <div className="mb-12.5 text-1xl font-semibold text-black dark:text-white">
                        Tutoriales
                      </div>

                      <div className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Chat con soporte
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div >
    </>
  );
};

export default Ayuda;
