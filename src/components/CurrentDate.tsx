import React from "react";

const CurrentDate = () => {
    const fechaActual = Date.now();
    const fechaFormateada = new Date(fechaActual).toLocaleDateString();
    return fechaFormateada;
};

export default CurrentDate